# Rx Retrofit Recycler

![Rx-Retrofit-Recycler](./mobile/src/main/res/mipmap-hdpi/ic_launcher.png "KoTiLogo")

Rx-Retrofit-Recycler is sample application (could be used as a template) showing combination RecyclerView with Retrofit based on Rx principles.

App code is based on [Android Templates And Utilities developed by Petr Nohejl](https://github.com/petrnohejl/Android-Templates-And-Utilities)
and also based on [RxJava Pagination Sample developed by Eugene Matsyuk](https://github.com/matzuk/PaginationSample)


Features
============
Auto-loading RecyclerView connected to JSON REST API with respect to: 

* Android Coding Guidelines
* Stateful Layout
* Runtime Permissions
* Rx Approach

Release Notes
============
1.0.0.x - auto-loading list (with loaders) connection to [Kotopeky API](http://kotopeky.cz/api/kotinode/event) and [JSONPlaceholder API](http://jsonplaceholder.typicode.com/) <br>
1.0.1.x - Kotopeky API TLS secure[Kotopkey API](https://kotopeky.cz/api/kotinode/event). Self-signed keystore ready in KotoSSLTrust.
                                                                                             
                                                                                               
Documentation
=============


Dependencies
============
* [Android Support Library](http://developer.android.com/tools/support-library/index.html)
* [AppCompat](https://developer.android.com/reference/android/support/v7/appcompat/package-summary.html)
* [GSON](http://code.google.com/p/google-gson/)
* [OkHttp](https://github.com/square/okhttp)
* [Retrofit](https://github.com/square/retrofit)
* [Butterknife](https://github.com/JakeWharton/butterknife)
* [RxAndroid](https://github.com/ReactiveX/RxAndroid)
* [RxJavaMath](https://github.com/ReactiveX/RxJavaMath)
* [GradleRetroLambda](https://github.com/evant/gradle-retrolambda)

Testing
=======

x :-(

Developed by
============

[Michal Jeníček](https://www.linkedin.com/in/jenicekmichal)


License
=======

        Copyleft ©2015 Michal Jenicek

Screens
============


