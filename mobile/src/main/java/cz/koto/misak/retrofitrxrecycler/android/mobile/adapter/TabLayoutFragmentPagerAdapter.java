package cz.koto.misak.retrofitrxrecycler.android.mobile.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import cz.koto.misak.retrofitrxrecycler.android.mobile.fragment.AutoLoadingKoTiNodeEventRecyclerFragment;
import cz.koto.misak.retrofitrxrecycler.android.mobile.fragment.AutoLoadingTypiCodePhotoRecyclerFragment;
import cz.koto.misak.retrofitrxrecycler.android.mobile.fragment.AutoLoadingTypiCodeUserRecyclerFragment;


public class TabLayoutFragmentPagerAdapter extends FragmentPagerAdapter {
    public static final int FRAGMENT_COUNT = 3;


    public TabLayoutFragmentPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }


    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return AutoLoadingKoTiNodeEventRecyclerFragment.newInstance("EVENT");
            case 1:
                return AutoLoadingTypiCodeUserRecyclerFragment.newInstance("USER");
            case 2:
                return AutoLoadingTypiCodePhotoRecyclerFragment.newInstance("PHOTO");
            default:
                return null;
        }
    }


    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "EVENT";
            case 1:
                return "USER";
            case 2:
                return "PHOTO";
            default:
                return null;
        }
    }


    public void refill() {
        notifyDataSetChanged();
    }
}
