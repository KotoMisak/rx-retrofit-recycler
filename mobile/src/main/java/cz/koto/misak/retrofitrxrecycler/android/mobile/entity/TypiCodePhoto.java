package cz.koto.misak.retrofitrxrecycler.android.mobile.entity;


import com.google.gson.annotations.SerializedName;

public class TypiCodePhoto {

    @SerializedName("id")
    Integer mId;

    @SerializedName("albumId")
    Integer mAlbumId;

    @SerializedName("title")
    String mTitle;

    @SerializedName("thumbnailUrl")
    String mThumbnailUrl;

    public TypiCodePhoto() {
    }

    public TypiCodePhoto(Integer id, String title) {
        mId = id;
        mTitle = title;
    }

    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }

    public Integer getmAlbumId() {
        return mAlbumId;
    }

    public void setmAlbumId(Integer mAlbumId) {
        this.mAlbumId = mAlbumId;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmThumbnailUrl() {
        return mThumbnailUrl;
    }

    public void setmThumbnailUrl(String mThumbnailUrl) {
        this.mThumbnailUrl = mThumbnailUrl;
    }
}
