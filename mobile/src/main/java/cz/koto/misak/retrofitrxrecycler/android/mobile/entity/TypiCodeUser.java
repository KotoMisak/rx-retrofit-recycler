package cz.koto.misak.retrofitrxrecycler.android.mobile.entity;


import com.google.gson.annotations.SerializedName;

public class TypiCodeUser {

    @SerializedName("id")
    Integer mId;

    @SerializedName("username")
    String mUsername;

    @SerializedName("email")
    String mEmail;

    public TypiCodeUser() {
    }

    public TypiCodeUser(Integer id, String userName) {
        mId = id;
        mUsername = userName;
    }

    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }

    public String getmUsername() {
        return mUsername;
    }

    public void setmUsername(String mUsername) {
        this.mUsername = mUsername;
    }

    public String getmEmail() {
        return mEmail;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }
}
