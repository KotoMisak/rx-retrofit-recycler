package cz.koto.misak.retrofitrxrecycler.android.mobile.rest;


import java.util.List;

import cz.koto.misak.retrofitrxrecycler.android.mobile.entity.KoTiEvent;
import cz.koto.misak.retrofitrxrecycler.android.mobile.entity.TypiCodePhoto;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface KoTiNodeApi {


    @GET("/api/kotinode/event")
    Observable<List<KoTiEvent>> eventList(@Query("offset") int offset,@Query("limit") int limit);

}
