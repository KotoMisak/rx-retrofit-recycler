package cz.koto.misak.retrofitrxrecycler.android.mobile.rest;


import java.util.List;

import cz.koto.misak.retrofitrxrecycler.android.mobile.entity.KoTiEvent;
import cz.koto.misak.retrofitrxrecycler.android.mobile.entity.TypiCodePhoto;
import cz.koto.misak.retrofitrxrecycler.android.mobile.entity.TypiCodeUser;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;


/**
 * Paging:
 * http://jsonplaceholder.typicode.com/photos/?_start=20&_limit=3
 * http://jsonplaceholder.typicode.com/photos/?_start=20&_end=30
 *
 * Last 30 items:
 * http://jsonplaceholder.typicode.com/photos/?_start=0&_end=30&_sort=id&_order=DESC
 *
 * Second item:
 * http://jsonplaceholder.typicode.com/photos/2c
 *
 *
 * For complex querying use:
 * @QueryMap Map<String, String> options
 */
public interface TypicodeApi {

    @GET("/photos")
    Observable<List<TypiCodePhoto>> photoList(@Query("_start") int offset,@Query("_limit") int limit);

    @GET("/photos/{id}")
    Observable<TypiCodePhoto> photo(@Path("id") int id);

    @GET("/users")
    Observable<List<TypiCodeUser>> userList(@Query("_start") int offset,@Query("_limit") int limit);

    @GET("/users/{id}")
    Observable<TypiCodeUser> user(@Path("id") int id);

}
