package cz.koto.misak.retrofitrxrecycler.android.mobile.rest;

import com.google.gson.GsonBuilder;

import cz.koto.misak.retrofitrxrecycler.android.mobile.RetrofitRxRecyclerConfig;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

public class TypicodeClient {

    private static TypicodeApi restInterface;

    public static TypicodeApi getJsonPlaceHolderClient() {

        if (restInterface == null) {
            GsonBuilder gsonBuilder = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sss'Z'");//ISO-8601
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(RetrofitRxRecyclerConfig.API_TYPICODE_ENDPOINT_PROD)
                    .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())//important for RX!!!
                    .build();

            restInterface =  retrofit.create(TypicodeApi.class);
        }
        return restInterface;
    }
}